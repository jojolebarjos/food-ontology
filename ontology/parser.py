
from parsy import generate, regex, string, success

from .data import Value, Parameter, Definition, Alias


# Define whitespace
whitespace = regex(r' *').desc('whitespace')
newline = regex(r' *(#[^\n]*)?\r*\n').desc('newline')
breakpoint = newline.many() >> whitespace

# Whitespace is ignore after tokens
lexeme = lambda p: p << whitespace

# An identifier is can have letters and digits
identifier = lexeme(regex(r'[a-zA-Z]\w*').desc('identifier'))

# Define symbols
lparen = lexeme(string('(')) << breakpoint
rparen = lexeme(string(')'))
colon = lexeme(string(':')) << breakpoint
comma = lexeme(string(',')) << breakpoint
equal = lexeme(string('=')) << breakpoint

# A value is an identifier, optionally with arguments
@generate
def value():
    identifier_ = yield identifier
    arguments_ = yield (lparen >> values << rparen).optional()
    return Value(identifier_, arguments_)

# Shorthand for many values
values = value.sep_by(comma)

# A parameter is an identifier, optionally with a type and/or a default value
@generate
def parameter():
    identifier_ = yield identifier
    type_ = yield (colon >> value).optional()
    default_ = yield (equal >> value).optional()
    return Parameter(identifier_, type_, default_)

# Shorthand for many parameters
parameters = parameter.sep_by(comma)

# A definition is an identifier, a list of parameters and a list of parents
@generate
def definition():
    identifier_ = yield identifier
    parameters_ = yield (lparen >> parameters << rparen).optional()
    parents_ = yield (colon >> values).optional()
    yield newline
    return Definition(identifier_, parameters_, parents_)

# An alias is an identifier, a list of parameters and a replacement value
@generate
def alias():
    identifier_ = yield identifier
    parameters_ = yield (lparen >> parameters << rparen).optional()
    value_ = yield equal >> value
    yield newline
    return Alias(identifier_, parameters_, value_)

# Many definitions form a document
entry = definition | alias
document = breakpoint >> (entry << breakpoint).many()
