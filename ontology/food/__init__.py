
import io
import os

from ..parser import document as document_parser
from ..domain import Domain


# Enumerate ontology files
def iterate_files(folder):
    for name in os.listdir(folder):
        path = os.path.join(folder, name)
        if path.endswith('.tx'):
            yield path
        elif os.path.isdir(path):
            yield from iterate_files(path)


# Load all ontology files as domain
def load_entries(paths):
    all_entries = []
    for path in paths:
        with io.open(path, 'r', encoding='utf-8') as file:
            data = file.read()
        entries = document_parser.parse(data)
        all_entries.extend(entries)
    return all_entries


# Use local files
here = os.path.dirname(os.path.abspath(__file__))
domain = Domain(load_entries(iterate_files(here)))

# Only export the domain object
__all__ = ['domain']
