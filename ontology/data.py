
from __future__ import annotations

from pydantic.dataclasses import dataclass

from typing import List, Optional


# TODO probably store location, using mark, which can be reported during analysis


@dataclass
class Value:
    identifier: str
    arguments: Optional[List[Value]] = None

Value.__pydantic_model__.update_forward_refs()


@dataclass
class Parameter:
    identifier: str
    type: Optional[Value] = None
    default: Optional[Value] = None


@dataclass
class Definition:
    identifier: str
    parameters: Optional[List[Parameter]] = None
    parents: Optional[List[Value]] = None


@dataclass
class Alias:
    identifier: str
    parameters: Optional[List[Parameter]]
    value: Value
