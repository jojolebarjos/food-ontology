
import itertools

from .data import Value, Definition, Alias

from .parser import (
    document as document_parser,
    value as value_parser
)


# Substitute identifiers using specified map
def replace(value, map):
    
    # Substitute identifier, if needed
    identifier = value.identifier
    replacement = map.get(identifier)
    if replacement is not None:
        
        # Check if arguments are also overridden
        if replacement.arguments:
            if value.arguments:
                raise ValueError(f'"{replacement.identifier}(...)" cannot fulfill "{value.identifier}", as it is already specialized')
            return replacement
        
        # Otherwise, just keep identifier
        identifier = replacement.identifier
    
    # Recurse to also substitute references in arguments
    if value.arguments is None:
        arguments = value.arguments
    else:
        arguments = [replace(argument, map) for argument in value.arguments]
    
    # Build new value
    return Value(identifier, arguments)


# Base class for all ontology types
class Type:
    def __init__(self, domain, identifier):
        self._domain = domain
        self._identifier = identifier
    
    @property
    def domain(self):
        return self._domain
    
    @property
    def identifier(self):
        return self._identifier
    
    @property
    def arguments(self):
        return None
    
    def __call__(self, *arguments):
        raise TypeError('"{self}" cannot be specialized')
    
    def is_a(self, right):
        right = self._domain(right)
        return right._is_a_right(self)
    
    def _is_a_right(self, left):
        return left._is_a_left(self)
    
    def _is_a_left(self, right):
        raise NotImplementedError()
    
    def __str__(self):
        if self.arguments is None:
            return self.identifier
        return f'{self.identifier}({", ".join(str(argument) for argument in self.arguments)})'
    
    def __repr__(self):
        return str(self)


# User-defined type, based on a definition
class DefinitionType(Type):
    def __init__(self, domain, definition, arguments=None):
        super().__init__(domain, definition.identifier)
        self._definition = definition
        self._specialized = arguments is not None
        self._arguments = arguments
        self._parents = None
        self._resolved = False
        
        # In case of specialization, check
        if self._specialized:
            if definition.parameters is None:
                raise TypeError(f'"{definition.identifier}" cannot be specialized')
            if len(arguments) > len(definition.parameters):
                raise ValueError(f'Too many arguments for "{definition.identifier}"')
    
    @property
    def arguments(self):
        return self._arguments
    
    # Using a two-step construction, to allow out-of-order definitions and potentially some acceptable cycles
    def _resolve(self):
        if not self._resolved:
            parameter_map = {}
            
            # If this class does not have any parameter, just ignore this step
            if self._definition.parameters is None:
                resolved_arguments = None
            
            # Or, if this class is not specialized, use argument type (default value is ignored in bare types)
            elif self._arguments is None:
                resolved_arguments = []
                for parameter in self._definition.parameters:
                    argument = parameter.type or Value('any')
                    argument = replace(argument, parameter_map)
                    parameter_map[parameter.identifier] = argument
                    argument = self._domain(argument)
                    resolved_arguments.append(argument)
            
            # Otherwise, this class is specialized
            else:
                resolved_arguments = []
                for parameter, argument in itertools.zip_longest(self._definition.parameters, self._arguments):
                    argument = argument or parameter.default or parameter.type or Value('any')
                    argument = replace(argument, parameter_map)
                    parameter_map[parameter.identifier] = argument
                    argument = self._domain(argument)
                    resolved_arguments.append(argument)
            
            # Resolve parents
            resolved_parents = []
            if self._definition.parents:
                for parent in self._definition.parents:
                    parent = replace(parent, parameter_map)
                    parent = self._domain(parent)
                    # TODO check for 'none' parent?
                    resolved_parents.append(parent)
            
            # Store resolved values
            self._arguments = resolved_arguments
            self._parents = resolved_parents
            self._resolved = True
    
    # Specialize type
    def __call__(self, *arguments):
        # TODO also accept strings
        if self._specialized:
            raise TypeError(f'"{self}" cannot be further specialized')
        result = DefinitionType(self._domain, self._definition, arguments)
        result._resolve()
        return result
    
    def _is_a_left(self, right):
        assert type(right) == DefinitionType
        
        # If other has same type, check arguments
        if self.identifier == right.identifier:
            if self._definition.parameters is None:
                return True
            for l, r in zip(self._arguments, right._arguments):
                if not l.is_a(r):
                    return False
            return True
        
        # Otherwise, at least one parent must agree
        if self._parents:
            for parent in self._parents:
                if parent.is_a(right):
                    return True
        return False


# User-defined type, based on aliasing
class AliasType(Type):
    def __init__(self, domain, alias, arguments=None):
        super().__init__(domain, alias.identifier)
        self._alias = alias
        self._specialized = arguments is not None
        self._arguments = arguments
        self._value = None
        self._resolved = False
        
        # In case of specialization, check
        if self._specialized:
            if alias.parameters is None:
                raise TypeError(f'"{alias.identifier}" cannot be specialized')
            if len(arguments) > len(alias.parameters):
                raise ValueError(f'Too many arguments for "{alias.identifier}"')
    
    @property
    def arguments(self):
        return self._arguments
    
    # Using a two-step construction, to allow out-of-order definitions and potentially some acceptable cycles
    def _resolve(self):
        if not self._resolved:
            parameter_map = {}
            
            # If this class does not have any parameter, just ignore this step
            if self._alias.parameters is None:
                resolved_arguments = None
            
            # Or, if this class is not specialized, use argument type (default value is ignored in bare types)
            elif self._arguments is None:
                resolved_arguments = []
                for parameter in self._alias.parameters:
                    argument = parameter.type or Value('any')
                    argument = replace(argument, parameter_map)
                    parameter_map[parameter.identifier] = argument
                    argument = self._domain(argument)
                    resolved_arguments.append(argument)
            
            # Otherwise, this class is specialized
            else:
                resolved_arguments = []
                for parameter, argument in itertools.zip_longest(self._alias.parameters, self._arguments):
                    argument = argument or parameter.default or parameter.type or Value('any')
                    argument = replace(argument, parameter_map)
                    parameter_map[parameter.identifier] = argument
                    argument = self._domain(argument)
                    resolved_arguments.append(argument)
            
            # Resolve value
            value = replace(self._alias.value, parameter_map)
            value = self._domain(value)
            resolved_value = value
            
            # Store resolved values
            self._arguments = resolved_arguments
            self._value = resolved_value
            self._resolved = True
    
    # Specialize type
    def __call__(self, *arguments):
        # TODO also accept strings
        if self._specialized:
            raise TypeError(f'"{self}" cannot be further specialized')
        result = AliasType(self._domain, self._alias, arguments)
        result._resolve()
        return result
    
    def _is_a_right(self, left):
        return left.is_a(self._value)
    
    def _is_a_left(self, right):
        return self._value.is_a(right)


# Super type of all types
class AnyType(Type):
    def __init__(self, domain):
        super().__init__(domain, 'any')
    
    def _is_a_right(self, left):
        return True
    
    def _is_a_left(self, right):
        return False
    


# Sub type of all types
class NoneType(Type):
    def __init__(self, domain):
        super().__init__(domain, 'none')
    
    def _is_a_right(self, left):
        return left.identifier == 'none'
    
    def _is_a_left(self, right):
        return True



# Combine multiple types, which must be all fulfilled
class AndType(Type):
    def __init__(self, domain, arguments=None):
        super().__init__(domain, 'and')
        self._arguments = arguments
        # TODO properly define behaviour on empty arguments
    
    @property
    def arguments(self):
        return self._arguments
    
    def __call__(self, *arguments):
        if self._arguments is not None:
            raise TypeError(f'"{self}" cannot be further specialized')
        return AndType(self.domain, [self.domain(argument) for argument in arguments])
    
    def _is_a_right(self, left):
        if self._arguments:
            for argument in self._arguments:
                if not left.is_a(argument):
                    return False
        return True
    
    def _is_a_left(self, right):
        if self._arguments:
            for argument in self._arguments:
                if not argument.is_a(right):
                    return False
        return True


# Combine multiple types, where at least one must be fulfilled
class OrType(Type):
    def __init__(self, domain, arguments=None):
        super().__init__(domain, 'or')
        self._arguments = arguments
        # TODO properly define behaviour on empty arguments
    
    @property
    def arguments(self):
        return self._arguments
    
    def __call__(self, *arguments):
        if self._arguments is not None:
            raise TypeError(f'"{self}" cannot be further specialized')
        return OrType(self.domain, [self.domain(argument) for argument in arguments])
    
    def _is_a_right(self, left):
        if self._arguments:
            for argument in self._arguments:
                if left.is_a(argument):
                    return True
        return False
    
    def _is_a_left(self, right):
        if self._arguments:
            for argument in self._arguments:
                if argument.is_a(right):
                    return True
        return False


# Type container
class Domain:
    def __init__(self, entries):
        
        # Register built-in types
        self._types = {
            'any': AnyType(self),
            'none': NoneType(self),
            'and': AndType(self),
            'or': OrType(self)
        }
        
        # Register custom types
        custom_types = []
        for entry in entries:
            if entry.identifier in self._types:
                raise ValueError(f'"{entry.identifier}" is already defined')
            if type(entry) is Alias:
                custom_type = AliasType(self, entry)
            else:
                custom_type = DefinitionType(self, entry)
            self._types[entry.identifier] = custom_type
            custom_types.append(custom_type)
        
        # Once all definitions are recorded, resolve values
        for custom_type in custom_types:
            custom_type._resolve()
    
    def __getattr__(self, identifier):
        return self._types[identifier]
    
    def __dir__(self):
        attributes = set(super().__dir__())
        attributes.update(self._types.keys())
        return attributes
    
    def __call__(self, value):
        
        # Allow None
        if value is None:
            return self.none
        
        # Skip, if it is already an instance
        if isinstance(value, Type):
            assert value.domain == self, 'Cross-domain instance is forbidden'
            return value
        
        # Parse text, if needed
        if type(value) is str:
            value = value_parser.parse(value)
        
        # Retrieve instance
        result = self._types[value.identifier]
        if value.arguments is not None:
            result = result(*value.arguments)
        return result
