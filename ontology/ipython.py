
from IPython.core.magic import Magics, magics_class, line_magic


@magics_class
class DomainMagics(Magics):
    def __init__(self, shell, domain):
        super().__init__(shell)
        self.domain = domain
    
    @line_magic
    def parse(self, line):
        return self.domain(line)


# Run in IPython:
#   %load_ext ontology.ipython
def load_ipython_extension(ipython):
    from .food import domain
    magics = DomainMagics(ipython, domain)
    ipython.register_magics(magics)
