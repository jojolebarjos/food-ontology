
from ontology.domain import *

with open('test.tx', 'r', encoding='utf-8') as file:
    text = file.read()

entries = document_parser.parse(text)

domain = Domain(entries)

i = domain('juice(lemon)')
j = domain('derivate(citrus)')

i.is_a(j)

