
from setuptools import find_packages, setup

setup(
    
    name = 'ontology',
    version = '0.0.1',
    
    packages = find_packages(),
    
    package_data = {'': ['*.tx']},
    
    author = 'Jojo le Barjos',
    author_email = 'jojolebarjos@gmail.com',
    license_file = 'LICENSE',
    
    description = 'Parametric taxonomy library, for food items',
    
    keywords = [
        'taxonomy',
        'ontology',
        'food',
        'recipe'
    ],
    
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: Freely Distributable',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.7',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    
    install_requires = [
        'parsy',
        'pydantic'
    ]
    
)
