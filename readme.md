
# Food Ontology

This ontology is designed to handle culinary needs. Biological, chemical and nutritional components are also considered to improve usability and reliability. Items are mostly everything, from raw ingredients to compound dishes. This is not limited to edible concepts (e.g. brands and processing indications).

Together, a directed acyclic graph is built, as commonly done in ontologies. For simplicity sake's, a custom format with weak annotation is used, which is closer to thesauri formalism than RDF-based ontologies. In particular, a parametric system is defined, where items can have arguments which dynamically impact the hierarchy. In a sense, it is similar to in programming languages such as Java.

```yaml
# Top-level definitions (i.e. implicit children of `any`, the abstract base class of everything)
fruit
beverage

# Direct children (i.e. a `lemon` is a `fruit`)
citrus : fruit
lemon : citrus
lime : citrus

# A top-level parametric type
# The actual meaning of a `derivate` is not specified by the ontology
derivate(source)

# A child can also inherit from parametric types
# For convenience, its parameter is also called `source`
# However, it is actually distinct from the one in `derivate`
juice(source) : derivate(source), beverage

# Specialization can be done at runtime, to specify parameters
# As parameters can be used in inheritance list, this will cascade through the whole hierarchy
# Therefore, `juice(lime)` is a `derivate(citrus)`
# Arguments must be compatible (a.k.a. type covariance)
```


## Installation

```
pip install https://gitlab.com/jojolebarjos/food-ontology.git
```
